//
//  UserListTableViewCell.swift
//  UserTest
//
//  Created by Rahul Kumar Patel on 4/3/18.
//  Copyright © 2018 Rahul Kumar Patel. All rights reserved.
//

import UIKit

class UserListTableViewCell: UITableViewCell {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userID: UILabel!
    @IBOutlet weak var profilePhoto: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(userNameStr: String, userIDStr: String, profilePhotoData: Data?) {
        self.userName.text = "UserName:= " + userNameStr
        self.userID.text = "User-Id:= " + userIDStr
        if let imageData = profilePhotoData {
            self.profilePhoto.image = UIImage(data: imageData)
        }
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
