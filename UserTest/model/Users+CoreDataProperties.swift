//
//  Users+CoreDataProperties.swift
//  UserTest
//
//  Created by Rahul Kumar Patel on 4/2/18.
//  Copyright © 2018 Rahul Kumar Patel. All rights reserved.
//
//

import Foundation
import CoreData


extension Users {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Users> {
        return NSFetchRequest<Users>(entityName: "Users")
    }

    @NSManaged public var name: String?
    @NSManaged public var userid: String?
    @NSManaged public var userPhoto: Data?
    @NSManaged public var password: String?

}
