//
//  ViewController.swift
//  UserTest
//
//  Created by Rahul Kumar Patel on 4/2/18.
//  Copyright © 2018 Rahul Kumar Patel. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    var viewModel: ViewModel? = ViewModel()
    var loginSuccess: Bool = false
    
    @IBOutlet weak var userPasswdTextF: UITextField!
    @IBOutlet weak var userNameTextF: UITextField!
    
    // view conroller lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextF.delegate = self
        userPasswdTextF.delegate = self
        viewModel?.readData()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintAdjustmentMode = .normal
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // on login button click
    @IBAction func loginAction(_ sender: Any) {
        userPasswdTextF.resignFirstResponder()
        userNameTextF.resignFirstResponder()
    }
    
    // error alert for sign up
    func showAlert() {
        let alert = UIAlertController(title: "login error", message: "It's recommended you to sign up with this user.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Sign-up", style: .default, handler: { (alert) in
            self.performSegue(withIdentifier: "SignUpViewController", sender: nil)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    // perform segue to check to invoke view controller
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        if let ident = identifier {
            if let usertext = self.userNameTextF.text?.count, ident == "UserListViewController" {
                if let userInput = self.userNameTextF.text, let passInput = self.userPasswdTextF.text,let success = viewModel?.checkForUserNameAndPasswordMatch(username: userInput, password: passInput), success , usertext > 0 {
                    return true
                }
                else {
                    showAlert()
                }
            }
        }
        if let ident = identifier , ident == "SignUpViewController" {
            return true
        }
        return false
    }
    
    // text field delegate 
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {// called when 'return' key pressed. return NO to ignore.
        textField.resignFirstResponder()
        return true
    }
    
}


