//
//  UserListViewController.swift
//  UserTest
//
//  Created by Rahul Kumar Patel on 4/3/18.
//  Copyright © 2018 Rahul Kumar Patel. All rights reserved.
//

import UIKit

class UserListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var viewModel: ViewModel? = ViewModel()
    var editButton: UIBarButtonItem = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "User List"
        viewModel?.readData()
        editButton = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(editTable))
        navigationItem.rightBarButtonItem = editButton

    }

    // edit / done navigation bar action
    @objc func editTable() {
        if self.tableView.isEditing {
            editButton.title = "Edit"
        }
        else {
            editButton.title = "Done"
        }
        self.tableView.isEditing = !self.tableView.isEditing
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
// uitable view data source and delegate methods
extension UserListViewController: UITableViewDataSource, UITableViewDelegate {
    //Necessary functions for basic functionality
    // number of section
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0 
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    // number of row in section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.people?.count ?? 0
    }
    
    // create cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //custom cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserListTableViewCell", for: indexPath) as? UserListTableViewCell else {
            return UITableViewCell()
        }
        guard let onePeople = self.viewModel?.people?[indexPath.row] else {
            return UITableViewCell()
        }
        let name = onePeople.name
        let userId = onePeople.userid
        let photo = onePeople.userPhoto
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.groupTableViewBackground
        } else {
            cell.backgroundColor = UIColor.white
        }
        // update the cell data from model
        cell.setData(userNameStr: name ?? "", userIDStr: userId ?? "", profilePhotoData: photo )
        return cell
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            guard let peoples = self.viewModel?.people else {return}
             let removeObj = peoples[indexPath.row]
            managedContext.delete(removeObj)
            self.viewModel?.people?.remove(at: indexPath.row)
            do {
                try managedContext.save()
                self.tableView.reloadData()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    // uitable view delegate method to navigation screen to detail
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewController: SignUpViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
            self.navigationController?.pushViewController(viewController, animated: true)
            guard let peoples = self.viewModel?.people else {return}
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { // refresh ui with detail
                viewController.setData((peoples[indexPath.row]))
            }
        }

    }
    
}
