//
//  SignUpViewController.swift
//  UserTest
//
//  Created by Rahul Kumar Patel on 4/2/18.
//  Copyright © 2018 Rahul Kumar Patel. All rights reserved.
//

import UIKit

// string extension for regex
extension String {
    func matchesRegex(regex: String!) -> Bool {
        var gotMatch = false
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let match = regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count))
            gotMatch =  (match != nil)
        } catch {
            gotMatch = false
        }
        return gotMatch
    }
    
}

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {
    // property method
    @IBOutlet weak var profilePhoto: UIButton!
    @IBOutlet weak var userIDTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userPasswdTextField: UITextField!
    @IBOutlet weak var userRePasswdTextField: UITextField!
    var viewModel: ViewModel? = ViewModel()
    
    @IBOutlet weak var labelRePasswdWarning: UILabel!
    @IBOutlet weak var lablePasswdWarning: UILabel!
    var picker: UIImagePickerController = UIImagePickerController()
    let pattern: String = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,12}$"
    
    // view controller life-cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "create account"
        userPasswdTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        userRePasswdTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        lablePasswdWarning.isHidden = true
        labelRePasswdWarning.isHidden = true
        picker.delegate=self
        profilePhoto.backgroundColor = .clear
        profilePhoto.layer.cornerRadius = 5
        profilePhoto.layer.borderWidth = 0.5
        profilePhoto.layer.borderColor = UIColor.gray.cgColor

        // Do any additional setup after loading the view.
    }
    
    // text field change text
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == self.userPasswdTextField {
            lablePasswdWarning.isHidden = false
            if let regexMatch = textField.text?.matchesRegex(regex: pattern), regexMatch {
                lablePasswdWarning.text = "Password accepted"
                lablePasswdWarning.textColor = UIColor.green
            }
            else {
                lablePasswdWarning.textColor = UIColor.red
                lablePasswdWarning.text = "password must consist of 1 letters & number between 5,12 character"
            }
        }
        else if textField == self.userRePasswdTextField {
            labelRePasswdWarning.isHidden = false
            if self.userPasswdTextField.text == self.userRePasswdTextField.text {
                labelRePasswdWarning.text = "Password matches"
                labelRePasswdWarning.textColor = UIColor.green
            }
            else {
                labelRePasswdWarning.textColor = UIColor.red
                labelRePasswdWarning.text = "password not match"
            }
        }
    }
    
    // to show detail screen from list view
    @objc func setData(_ user: Users) {
        self.title = "Detail"
        self.userNameTextField.text = user.name
        self.userIDTextField.text = user.userid
        self.userPasswdTextField.text = user.password
        self.userRePasswdTextField.text = user.password
        if let imageData = user.userPhoto {
            let image = UIImage(data: imageData)
            self.profilePhoto.setBackgroundImage(image, for: .normal)
        }
    }
    
    // to validate input data on button click
    @IBAction func createAccountAction(_ sender: Any) {
        if let text = self.userIDTextField.text, text.isEmpty  {
            self.viewModel?.showAlert(self, message: "User ID field is blank")
            return
        }
        if let text = self.userNameTextField.text, text.isEmpty  {
            self.viewModel?.showAlert(self, message: "User name field is blank")
            return
        }
        if let text = self.userPasswdTextField.text, text.isEmpty  {
            self.viewModel?.showAlert(self, message: "User password field is blank")
            return
        }
        if let text = self.userRePasswdTextField.text, text.isEmpty  {
            self.viewModel?.showAlert(self, message: "User re-Password field is blank")
            return
        }
        if let regexMatch = userPasswdTextField.text?.matchesRegex(regex: pattern), self.userPasswdTextField.text == self.userRePasswdTextField.text && regexMatch {
            var dict = Dictionary<String, Any>()
            dict["name"] = userNameTextField.text
            dict["userid"] = userIDTextField.text
            dict["password"] = userPasswdTextField.text
            if let image = profilePhoto.backgroundImage(for: .normal) {
                dict["userPhoto"] =  UIImageJPEGRepresentation(image, 0.1)
            }
            viewModel?.save(userDict: dict)
            self.navigationController?.popViewController(animated: true)

        }
        else {
            self.viewModel?.showAlert(self, message: "password must consist of 1 letters & number between 5,12 character")
        }
    }
    
    // change photo
    @IBAction func choosePhoto(_ sender: Any) {
        openCamera()
    }
    
    // open image gallary
    func openGallary()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    // open camera view
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }else{
            openGallary()
        }
    }
    
    // image picker delegate
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.profilePhoto.setBackgroundImage(image, for: .normal)
        }
        else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.profilePhoto.setBackgroundImage(image, for: .normal)
        } else{
            print("error")
        }
        dismiss(animated: true, completion: nil)
    }

    // cancel picker view
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // delegate uitext field
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {// called when 'return' key pressed. return NO to ignore.
        if textField == self.userPasswdTextField {
            if let regexMatch = textField.text?.matchesRegex(regex: pattern), regexMatch {
                lablePasswdWarning.text = "password valid"
                lablePasswdWarning.textColor = UIColor.green
            }
            else {
                lablePasswdWarning.textColor = UIColor.red
                lablePasswdWarning.text = "password must consist of 1 letters & number between 5,12 char"
            }
        }
        textField.resignFirstResponder()
        return true
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

