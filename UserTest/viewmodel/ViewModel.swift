//
//  ViewModel.swift
//  UserTest
//
//  Created by Rahul Kumar Patel on 4/2/18.
//  Copyright © 2018 Rahul Kumar Patel. All rights reserved.
//

import Foundation
import UIKit
import CoreData
public class ViewModel {
    var people: [Users]? = []

    
    // save user data on coredata
    func save(userDict: [String:Any]) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: "Users", in: managedContext) else { return }
        
        let person = NSManagedObject(entity: entity, insertInto: managedContext) as? Users
        person?.setValue(userDict["name"], forKeyPath: "name")
        person?.setValue(userDict["userid"], forKeyPath: "userid")
        person?.setValue(userDict["userPhoto"], forKeyPath: "userPhoto")
        person?.setValue(userDict["password"], forKeyPath: "password")
        
        do {
            guard let person = person else { return }
            try managedContext.save()
            people?.append(person)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    // to validate user id and password for login screen
    func checkForUserNameAndPasswordMatch( username: String, password : String) -> Bool
    {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Users")
        let predicate = NSPredicate(format: "userid = %@", username)
        fetchRequest.predicate = predicate
        do
        {
            let result = try managedContext.fetch(fetchRequest) as? [Users]
            if let count = result?.count, count > 0
            {
                let objectentity = result?.first
                if objectentity?.userid == username && objectentity?.password == password
                {
                    print("Login Succesfully")
                    return true
                }
                else
                {
                    print("Wrong username or password !!!")
                    return false
                }
            }
        }
            
        catch
        {
            let fetch_error = error as NSError
            print("error", fetch_error.localizedDescription)
        }
        return false
    }
    
    // show alert with generic message
    func showAlert(_ myView:UIViewController, message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        myView.present(alert, animated: true)
    }

    // read data from core data
    func readData() {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Users")
        
        do {
            people = try managedContext.fetch(fetchRequest) as? [Users]
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
